package mines;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Mines extends JFrame implements Serializable {

    private JLabel statusbar;
    private JLabel HP;
    private JLabel levelOfDifficulty;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private JLabel label5;
    private JLabel label6;
    private JLabel label7;
    private JButton solveGame;
    private JButton undoButton;
    private JButton redoButton;
    private JButton saveGameButton;
    private JPanel buttonPanel;
    private int difficulty;
    Board currentGame;

    public Mines(int dif, String playerName, int indexOfGamesPlayed) {

        this.difficulty = dif;

        String difString;
        int gridWidth, gridHeight;

        if (dif == 1) {
            difString = "Medium";
            gridWidth = 570;
            gridHeight = 400;
        } else if (dif == 2) {
            difString = "Hard";
            gridWidth = 640;
            gridHeight = 480;

        } else {
            difString = "Easy";
            gridWidth = 420;
            gridHeight = 270;
        }

        setSize(gridWidth, gridHeight);
        setTitle("Minesweeper");

        //////////////// Labels //////////////

        statusbar = new JLabel("");
        HP = new JLabel("");

        levelOfDifficulty = new JLabel(difString);

        label1 = new JLabel("Player:");
        label2 = new JLabel(playerName);
        label3 = new JLabel("Difficulty");
        label4 = new JLabel("Mines Left:");
        label5 = new JLabel("Game Controls");
        label6 = new JLabel("Solve Game:");
        label7 = new JLabel("Save Game:");

        ///////////////// Panels /////////////

        buttonPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(2, 2, 2, 2);

        //////////////// Buttons //////////////

        solveGame = new JButton("Solve");
        solveGame.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                currentGame.solveGame();
            }
        });

        undoButton = new JButton("Undo");
        undoButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {

                currentGame.decrementInstanceIndex();
            }
        });

        redoButton = new JButton("Redo");
        redoButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                currentGame.incrementIstanceIndex();
            }
        });

        saveGameButton = new JButton("Save");
        saveGameButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                saveGame();
            }
        });

        /////////// Button & Layer Panel Layout ///////////

        gbc.anchor = GridBagConstraints.LINE_START;

        gbc.gridx = 0;
        gbc.gridy = 0;
        buttonPanel.add(label1, gbc); // Player label

        gbc.gridx = 1;
        gbc.gridy = 0;
        buttonPanel.add(label2, gbc); // Player name

        gbc.gridx = 0;
        gbc.gridy = 1;
        buttonPanel.add(new JLabel("HP: "), gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        buttonPanel.add(HP, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        buttonPanel.add(label3, gbc); // Difficulty Label

        gbc.gridx = 1;
        gbc.gridy = 2;
        buttonPanel.add(levelOfDifficulty, gbc); // Easy, Medium or Hard

        gbc.gridx = 0;
        gbc.gridy = 3;
        buttonPanel.add(label4, gbc); // Mines left Label

        gbc.gridx = 1;
        gbc.gridy = 3;
        buttonPanel.add(statusbar, gbc); // Number of mines left

        gbc.gridx = 0;
        gbc.gridy = 4;
        buttonPanel.add(label5, gbc); // Game Control Label

        gbc.gridx = 0;
        gbc.gridy = 5;
        buttonPanel.add(undoButton, gbc); // Undo Button

        gbc.gridx = 1;
        gbc.gridy = 5;
        buttonPanel.add(redoButton, gbc); // Redo Button

        gbc.gridx = 0;
        gbc.gridy = 6;
        buttonPanel.add(label6, gbc); // Solve Game Label

        gbc.gridx = 1;
        gbc.gridy = 6;
        buttonPanel.add(solveGame, gbc); // Solve Game Button

        gbc.gridx = 0;
        gbc.gridy = 7;
        buttonPanel.add(label7, gbc); // Save Game Label

        gbc.gridx = 1;
        gbc.gridy = 7;
        buttonPanel.add(saveGameButton, gbc); // Save Game Button

        add(buttonPanel, BorderLayout.EAST);

        //////////////////////////////////////////////////////////
        //////////////////////// Game ////////////////////////////
        //////////////////////////////////////////////////////////

        currentGame = new Board(statusbar, HP, dif, playerName, indexOfGamesPlayed);
        add(currentGame);
        setResizable(false);
        setVisible(true);

        //////////////////////////////////////////////////////////
        ///////////////// Save Game upon closing//////////////////
        //////////////////////////////////////////////////////////

        //Saves players upon close.
        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                saveGame();
            }
        });

    } // END OF MINES CONSTRUCTOR

    //////////////////////////////////////////////////////////
    ///////////////// Accessors //////////////////////////////
    //////////////////////////////////////////////////////////
    public int getDifficulty() {
        return this.difficulty;

    }
    //Parses game result (true/false)

    public boolean getResult() {
        return currentGame.getGameResult();
    }
    //////////////////////////////////////////////////////////
    ///////////////// Methods ////////////////////////////////
    //////////////////////////////////////////////////////////

    public void saveGame() {
        currentGame.saveGame();
    }

    public void setMinesArray(int value, int i, int instance, int currentInstance, boolean end, int health) {
        currentGame.setLoadedInstance(value, i, instance, currentInstance, end, health);
    }

    public void setHealthArray(int value, int i) {
        currentGame.setHealthInstanceArray(value, i);
    }

    public void setMinesLeftArray(int value, int i) {
        currentGame.setMinesLeftInstanceArray(value, i);
    }
}
