package mines;

import java.awt.event.MouseAdapter;
import java.text.SimpleDateFormat;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import java.text.DateFormat;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.util.Random;
import java.awt.Image;
import java.util.Date;
import java.io.*;
import javax.swing.JOptionPane;

public final class Board extends JPanel implements Serializable {

    // Instance Variables 
    private final int NUM_IMAGES = 13;
    private final int CELL_SIZE = 15;
    private final int COVER_FOR_CELL = 10;
    private final int MARK_FOR_CELL = 10;
    private final int EMPTY_CELL = 0;
    private final int MINE_CELL = 9;
    private final int COVERED_MINE_CELL = MINE_CELL + COVER_FOR_CELL;
    private final int MARKED_MINE_CELL = COVERED_MINE_CELL + MARK_FOR_CELL;
    private final int DRAW_MINE = 9;
    private final int DRAW_COVER = 10;
    private final int DRAW_MARK = 11;
    private final int DRAW_WRONG_MARK = 12;
    private int[] field;
    private int instanceIndex;
    private boolean inGame;
    private boolean gameSolved;
    private int mines_left;
    private int mines;
    private int rows;
    private int cols;
    private int all_cells;
    private int gameNumber;
    //Variable to define the Game Result 
    private boolean gameWon;
    //Variable to define the difficulty 0 equals Easy, 1 equals 
    //Medium and 2 equals Hard
    protected int difficulty;
    transient private Image[] img;
    private Date gameDate;
    private JLabel statusbar;
    private JLabel healthStatus;
    private String playerName;
    private int[][] gameInstance;
    private int[] healthInstance;
    private int[] minesLeftInstance;
    private int health; //100 HP for all difficulties
    private int mineImpact; // 10 on easy, 25 on Medium and 50 on hard 

    public Board(JLabel statusbar, JLabel healthStatus, int difficulty, String playerName, int gameNumber) {

        this.difficulty = difficulty;
        this.statusbar = statusbar;
        this.playerName = playerName;
        this.gameNumber = gameNumber;
        this.instanceIndex = 0;
        this.gameSolved = false;
        this.healthStatus=healthStatus;

        img = new Image[NUM_IMAGES];
        // Sets date Format
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        //get current date time with Date()
        this.gameDate = new Date();

        for (int i = 0; i < NUM_IMAGES; i++) {
            img[i] = (new ImageIcon(this.getClass().getResource("j" + (i) + ".png"))).getImage();
        }

        setDoubleBuffered(true);

        addMouseListener(new MinesAdapter());
        
        newGame();
        
        gameInstance = new int[this.all_cells][];
        healthInstance=new int[this.all_cells];
        minesLeftInstance= new int[this.all_cells];
        
        saveInitialInstance(); // Only saves the initial state of the game.
        
        System.out.println("Game Created");
        System.out.println("On: " + dateFormat.format(gameDate));
        System.out.println("Player " + playerName + ". Game No: " + gameNumber);
        System.out.println(difficultyToString() + ". Mines left:" + getMinesLeft());
        System.out.println("");

    } // END OF BOARD CONSTRUCTOR

    //////////////////////////////////////////////////////////////////////////
    ///////////////////////////// Mutators ///////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    public void setDifficulty(int setDifficulty) {
        if (setDifficulty == 1) {
            mines = 100;
            cols = 25;
            rows = 25;
            mineImpact=25;
        } else if (setDifficulty == 2) {
            mines = 200;
            rows = 30;
            cols = 30;
            mineImpact=50;
        } else {
            mines = 40;
            rows = 16;
            cols = 16;
            mineImpact=10;
        }
    }
    
    public void setHealth(int health){
        this.health=health;
        healthStatus.setText(Integer.toString(this.health));
    }

    public void setGameResult(boolean value) {
        this.gameWon = value;
    }
    
    public void setHealthInstanceArray(int value, int i){
        this.healthInstance[i]=value;
        setHealth(healthInstance[i]);
    }
    
    public void setMinesLeftInstanceArray(int minesLeft, int i){
        this.minesLeftInstance[i]=minesLeft;
        setMinesLeft(minesLeftInstance[i]);
    }
    

    public void setLoadedInstance(int value, int i, int instance, int currentInstance, boolean endOfArray, int health) {

        if (gameInstance[instance] == null) {
            gameInstance[instance] = new int[field.length];
        }
        
        gameInstance[instance][i] = value;

        if ((endOfArray)) {
            for (int j = 0; j < field.length; j++) {
                field[j] = gameInstance[currentInstance][j];
                instanceIndex = currentInstance;
            }
            repaint();
        }

    }
    
    public void setMinesLeft(int value){
        this.mines_left=value;
        statusbar.setText(Integer.toString(this.mines_left));
    }

    // Will only be called when the instanceIndex =0;
    // Saves initial state of game.
    public void saveInitialInstance() {

        gameInstance[instanceIndex] = new int[field.length];

        for (int i = 0; i < field.length; i++) {
            gameInstance[instanceIndex][i] = field[i];
        }
    }

    // Saves an instance of the game in the gameInstance Array - MAIN MEMORY.
    public void saveGameInstance() {

        instanceIndex++;

        for (int i = instanceIndex; i < gameInstance.length; i++) {

            if (gameInstance[i] != null) {

                gameInstance[i] = null;
                healthInstance[i]=0;
                minesLeftInstance[i]=0;
                System.out.println("Game Instance in " + i + " eliminated");
            }
        }

        System.out.println("saved instanceIndex: " + instanceIndex);

        gameInstance[instanceIndex] = new int[field.length];

        for (int i = 0; i < field.length; i++) {
            gameInstance[instanceIndex][i] = field[i];
        }
        
        healthInstance[instanceIndex]=getHealth();
        minesLeftInstance[instanceIndex]=getMinesLeft();
    }

    public void incrementIstanceIndex() {
        
        this.instanceIndex++;
        
        if (gameInstance[instanceIndex] != null) {
            for (int i = 0; i < field.length; i++) {
                field[i] = gameInstance[instanceIndex][i];
            }
            
            setHealth(healthInstance[instanceIndex]);
            setMinesLeft(minesLeftInstance[instanceIndex]);
            
            repaint();
        } else {
            this.instanceIndex--;
            JOptionPane.showMessageDialog(null, "Cant Redo");
        }
        System.out.println("Current instanceIndex " + instanceIndex);

    }

    public void decrementInstanceIndex() {

        // Undo game solved thorgh solve Game function.
        if (gameSolved) {
            for (int i = 0; i < cols; i++) {
                for (int j = 0; j < rows; j++) {
                    if (field[(j * cols) + i] > MINE_CELL) {

                        field[(j * cols) + i] = +COVER_FOR_CELL;
                    }
                }
            }
            
            inGame = true;
            repaint();
            statusbar.setText(Integer.toString(getMinesLeft()));
        }

        // In case of lost game
        if (!inGame) {
            statusbar.setText(Integer.toString(getMinesLeft()));
            inGame = true;
        }

        if ((instanceIndex > 0)) {
            this.instanceIndex--;
            System.out.println("Decremented to instanceIndex " + instanceIndex);
            for (int i = 0; i < field.length; i++) {
                field[i] = gameInstance[instanceIndex][i];
            }
            setHealth(healthInstance[instanceIndex]);
            setMinesLeft(minesLeftInstance[instanceIndex]);
            repaint();
        } else {
            JOptionPane.showMessageDialog(null, "Cant Undo");
        }
    }

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////// Accessors ///////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    public int getMinesLeft() {
        return this.mines_left;
    }

    public int getDifficulty() {
        return this.difficulty;
    }

    public String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return dateFormat.format(this.gameDate);
    }

    public boolean getGameResult() {
        return this.gameWon;
    }
    
    public int getHealth(){
        return this.health;
    }
    
    

    //////////////////////////////////////////////////////////////////////////
    ///////////////////////// Class Methods //////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    public String toString() {
        return playerName + " " + gameNumber + " " + gameDate;
    }

    public String difficultyToString() {
        if (getDifficulty() == 0) {
            return "Difficulty: Easy";
        } else if (getDifficulty() == 1) {
            return "Difficulty: Medium";
        } else if (getDifficulty() == 2) {
            return "Difficulty: Hard";
        } else {
            return null;
        }

    }

    public void newGame() {

        Random random;
        int current_col;

        int i = 0;
        int position = 0;
        int cell = 0;

        setDifficulty(difficulty);

        random = new Random();
        inGame = true;
        setHealth(100);
        setMinesLeft(mines);

        all_cells = rows * cols;
        field = new int[all_cells];

        // Covers all cells
        for (i = 0; i < all_cells; i++) {
            field[i] = COVER_FOR_CELL;
        }

        this.statusbar.setText(Integer.toString(getMinesLeft()));
        this.healthStatus.setText(Integer.toString(getHealth()));

        i = 0;
        while (i < mines) {

            position = (int) (all_cells * random.nextDouble());

            if ((position < all_cells)
                    && (field[position] != COVERED_MINE_CELL)) {


                current_col = position % cols;
                field[position] = COVERED_MINE_CELL;
                i++;

                if (current_col > 0) {
                    cell = position - 1 - cols;
                    if (cell >= 0) {
                        if (field[cell] != COVERED_MINE_CELL) {
                            field[cell] += 1;
                        }
                    }
                    cell = position - 1;
                    if (cell >= 0) {
                        if (field[cell] != COVERED_MINE_CELL) {
                            field[cell] += 1;
                        }
                    }

                    cell = position + cols - 1;
                    if (cell < all_cells) {
                        if (field[cell] != COVERED_MINE_CELL) {
                            field[cell] += 1;
                        }
                    }
                }

                cell = position - cols;
                if (cell >= 0) {
                    if (field[cell] != COVERED_MINE_CELL) {
                        field[cell] += 1;
                    }
                }
                cell = position + cols;
                if (cell < all_cells) {
                    if (field[cell] != COVERED_MINE_CELL) {
                        field[cell] += 1;
                    }
                }

                if (current_col < (cols - 1)) {
                    cell = position - cols + 1;
                    if (cell >= 0) {
                        if (field[cell] != COVERED_MINE_CELL) {
                            field[cell] += 1;
                        }
                    }
                    cell = position + cols + 1;
                    if (cell < all_cells) {
                        if (field[cell] != COVERED_MINE_CELL) {
                            field[cell] += 1;
                        }
                    }
                    cell = position + 1;
                    if (cell < all_cells) {
                        if (field[cell] != COVERED_MINE_CELL) {
                            field[cell] += 1;
                        }
                    }
                }
            }
        }

    }

    public void find_empty_cells(int j) {

        int current_col = j % cols;
        int cell;

        if (current_col > 0) {
            cell = j - cols - 1;
            if (cell >= 0) {
                if (field[cell] > MINE_CELL) {
                    field[cell] -= COVER_FOR_CELL;
                    if (field[cell] == EMPTY_CELL) {
                        find_empty_cells(cell);
                    }
                }
            }

            cell = j - 1;
            if (cell >= 0) {
                if (field[cell] > MINE_CELL) {
                    field[cell] -= COVER_FOR_CELL;
                    if (field[cell] == EMPTY_CELL) {
                        find_empty_cells(cell);
                    }
                }
            }

            cell = j + cols - 1;
            if (cell < all_cells) {
                if (field[cell] > MINE_CELL) {
                    field[cell] -= COVER_FOR_CELL;
                    if (field[cell] == EMPTY_CELL) {
                        find_empty_cells(cell);
                    }
                }
            }
        }

        cell = j - cols;
        if (cell >= 0) {
            if (field[cell] > MINE_CELL) {
                field[cell] -= COVER_FOR_CELL;
                if (field[cell] == EMPTY_CELL) {
                    find_empty_cells(cell);
                }
            }
        }

        cell = j + cols;
        if (cell < all_cells) {
            if (field[cell] > MINE_CELL) {
                field[cell] -= COVER_FOR_CELL;
                if (field[cell] == EMPTY_CELL) {
                    find_empty_cells(cell);
                }
            }
        }

        if (current_col < (cols - 1)) {
            cell = j - cols + 1;
            if (cell >= 0) {
                if (field[cell] > MINE_CELL) {
                    field[cell] -= COVER_FOR_CELL;
                    if (field[cell] == EMPTY_CELL) {
                        find_empty_cells(cell);
                    }
                }
            }

            cell = j + cols + 1;
            if (cell < all_cells) {
                if (field[cell] > MINE_CELL) {
                    field[cell] -= COVER_FOR_CELL;
                    if (field[cell] == EMPTY_CELL) {
                        find_empty_cells(cell);
                    }
                }
            }

            cell = j + 1;
            if (cell < all_cells) {
                if (field[cell] > MINE_CELL) {
                    field[cell] -= COVER_FOR_CELL;
                    if (field[cell] == EMPTY_CELL) {
                        find_empty_cells(cell);
                    }
                }
            }
        }

    }

    public void solveGame() {

        //Goes through the array to change the cover of each cell.

        for (int i = 0; i < cols; i++) {
            for (int j = 0; j < rows; j++) {
                if (field[(j * cols) + i] > MINE_CELL) {

                    field[(j * cols) + i] -= COVER_FOR_CELL;
                }
            }
        }
        /* LEFT INTENTIONALLY FOR TESTING
         * UNCOMENT THIS SECTION TO QUICKLY ADD WON GAMES TO PLAYERS STATISTICS
         for (int i = 0; i < cols; i++) { for (int j = 0; j < rows; j++) { if
         (field[(j * cols) + i] == MINE_CELL) {
        
         field[(j * cols) + i] += DRAW_MARK; } }
        }*/
         

        saveGameInstance();
        // SET TO false TO QUICKLY ADD WON GAMES TO PLAYERS STATISTICS
        gameSolved = true;
        repaint();
    }

    /// SAVES TO DISK
    public void saveGame() {

        String fileName = playerName + "-" + gameNumber + ".txt";
        File saveGameFile = new File(fileName);
        int i = 0;
        int k = 0;
        int length;

        try {
            BufferedWriter fw = new BufferedWriter(new FileWriter(saveGameFile));

            // Writes to file the parameters needed to load game.
            fw.write(playerName); // Writes name of player to file.
            fw.newLine();
            fw.write(Integer.toString(gameNumber)); // Writes games number to file
            fw.newLine();
            fw.write(Integer.toString(difficulty)); // Writes difficulty to file
            fw.newLine();
            length = gameInstance[i].length;
            fw.write(Integer.toString(length)); // Writes length of instance array
            fw.newLine();

            while (gameInstance[k] != null) { // Determines how many instances exist
                k++;
            }

            fw.write(Integer.toString(k)); // Writes number of instances to be saved
            fw.newLine();
            fw.write(Integer.toString(instanceIndex)); // Writes current Instance Index
            fw.newLine();
            fw.write(Integer.toString(health)); //Writes current Health;
            fw.newLine();

            while (gameInstance[i] != null) { //While there is an instance do:

                for (int j = 0; j < length; j++) {
                    fw.write(Integer.toString(gameInstance[i][j]));
                    fw.newLine();
                }

                i++;
            }

            for (int j=0; j<i; j++){
                fw.write(Integer.toString(healthInstance[j]));
                fw.newLine();
            }
            
            for (int j=0; j<i; j++){
                fw.write(Integer.toString(minesLeftInstance[j]));
                fw.newLine();
            }
            
            fw.close();

            System.out.print(fileName + " saved to disk. ");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        System.out.println(playerName + " Game " + gameNumber + " Saved");
        System.out.println("");
    }

    class MinesAdapter extends MouseAdapter {

        public void mousePressed(MouseEvent e) {

            int x = e.getX();
            int y = e.getY();

            int cCol = x / CELL_SIZE;
            int cRow = y / CELL_SIZE;

            boolean rep = false;


            if (!inGame) {
                repaint();
            }


            if ((x < cols * CELL_SIZE) && (y < rows * CELL_SIZE)) {

                if (e.getButton() == MouseEvent.BUTTON3) {

                    if (field[(cRow * cols) + cCol] > MINE_CELL) {
                        rep = true;

                        if (field[(cRow * cols) + cCol] <= COVERED_MINE_CELL) {
                            if (getMinesLeft() > 0) {
                                field[(cRow * cols) + cCol] += MARK_FOR_CELL;
                                setMinesLeft(getMinesLeft()-1);
                                statusbar.setText(Integer.toString(getMinesLeft()));
                            } else {
                                statusbar.setText("No marks left");
                            }
                        } else {

                            field[(cRow * cols) + cCol] -= MARK_FOR_CELL;
                            setMinesLeft(getMinesLeft()+1);
                            statusbar.setText(Integer.toString(getMinesLeft()));
                        }
                    }

                    ////// Saves the instance of the game in a new int[]
                    saveGameInstance();

                } else {

                    if (field[(cRow * cols) + cCol] > COVERED_MINE_CELL) {
                        return;
                    }

                    if ((field[(cRow * cols) + cCol] > MINE_CELL)
                            && (field[(cRow * cols) + cCol] < MARKED_MINE_CELL)) {

                        field[(cRow * cols) + cCol] -= COVER_FOR_CELL;
                        rep = true;

                        if (field[(cRow * cols) + cCol] == MINE_CELL) {
                            
                            if (getHealth()>0){
                            setHealth(getHealth()-mineImpact);
                            setMinesLeft(getMinesLeft()-1);
                            statusbar.setText(Integer.toString(getMinesLeft()));
                            healthStatus.setText(Integer.toString(getHealth()));
                            } else return;
                            
                            if (getHealth()==0){
                            inGame = false;
                            }
                        
                        }
                        if (field[(cRow * cols) + cCol] == EMPTY_CELL) {
                            find_empty_cells((cRow * cols) + cCol);
                        }
                    }

                    ////// Saves the instance of the game in a new int[]
                    saveGameInstance();

                }

                if (rep) {
                    repaint();
                }

            }
        }
    }

    public void paint(Graphics g) {

        int cell = 0;
        int uncover = 0;


        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {

                cell = field[(i * cols) + j];

                if (inGame && cell == MINE_CELL) {
                    if (getHealth() == 0) {
                        inGame = false;
                    }
                }

                if (!inGame) {
                    if (cell == COVERED_MINE_CELL) {
                        cell = DRAW_MINE;
                    } else if (cell == MARKED_MINE_CELL) {
                        cell = DRAW_MARK;
                    } else if (cell > COVERED_MINE_CELL) {
                        cell = DRAW_WRONG_MARK;
                    } else if (cell > MINE_CELL) {
                        cell = DRAW_COVER;
                    }


                } else {
                    if (cell > COVERED_MINE_CELL) {
                        cell = DRAW_MARK;
                    } else if (cell > MINE_CELL) {
                        cell = DRAW_COVER;
                        uncover++;
                    }
                }

                g.drawImage(img[cell], (j * CELL_SIZE),
                        (i * CELL_SIZE), this);
            }
        }


        if (uncover == 0 && (inGame) && !gameSolved) {
            inGame = false;
            statusbar.setText("Game won");
            setGameResult(true);

        } else if (!inGame && !gameSolved) {
            statusbar.setText("Game lost");
            setGameResult(false);

        } else if (gameSolved) {
            statusbar.setText("Game Solved");
            setGameResult(false);
        }
    }
}
