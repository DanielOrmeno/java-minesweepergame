package mines;

import java.io.Serializable;

/**
 *
 * @author dormenog
 */
public class Player implements Serializable {

    protected String playerName;
    protected Mines[] gamesPlayed = new Mines[200];
    protected int indexOfGamesPlayed;
    protected int gamesStarted;
    protected int easyGamesWon;
    protected int mediumGamesWon;
    protected int hardGamesWon;
    protected int easyGamesLost;
    protected int mediumGamesLost;
    protected int hardGamesLost;

    public Player(String playerName, int difficulty) {
        this.playerName = playerName;
        indexOfGamesPlayed = 0;
        System.out.println("Player " + playerName + " has been created");
        System.out.println("");
        newGame(difficulty);

    } // End of Player Constructor

    //////////////////////////////////////////////////////////////////////////
    ///////////////////////////// Accessors //////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    public String getPlayerName() {
        return playerName;
    }

    public int getIndex() {
        return indexOfGamesPlayed;
    }

    public int getEasyGamesWon() {
        return this.easyGamesWon;
    }

    public int getEasyGamesLost() {
        return this.easyGamesLost;
    }

    public int getMediumGamesWon() {
        return this.mediumGamesWon;
    }

    public int getMediumGamesLost() {
        return this.mediumGamesLost;
    }

    public int getHardGamesWon() {
        return this.hardGamesWon;
    }

    public int getHardGamesLost() {
        return this.hardGamesLost;
    }

    public String getStatistics() {
        setStatistics();
        return "Number of Games Played: " + gamesStarted + "\n"
                + "Number of Easy games won: " + easyGamesWon + "\n"
                + "Number of Easy games lost: " + easyGamesLost + "\n"
                + "Number of Medium games won: " + mediumGamesWon + "\n"
                + "Number of Medium games lost: " + mediumGamesLost + "\n"
                + "Number of Hard games won: " + hardGamesWon + "\n"
                + "Number of Hard games lost: " + hardGamesLost + "\n";
    } // End of getStatistics Method

    //////////////////////////////////////////////////////////////////////////
    ///////////////////////////// Mutators ///////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    public void newGame(int difficulty) {
        gamesPlayed[indexOfGamesPlayed] = new Mines(difficulty, this.playerName, indexOfGamesPlayed);
        indexOfGamesPlayed++;
    }

    public void loadGame(int gameNumber, int difficulty) {
        gamesPlayed[gameNumber] = new Mines(difficulty, this.playerName, gameNumber);
    }

    public void setPlayerArray(int gameNumber, int value, int i, int instance, int currentInstance, boolean end, int health) {
        gamesPlayed[gameNumber].setMinesArray(value, i, instance, currentInstance, end, health);
    }
    
    public void setPlayerHealthInstance(int gameNumber, int value, int i){
        gamesPlayed[gameNumber].setHealthArray(value, i);
    }
    
    public void setPlayerMinesLeftInstance(int gameNumber, int value, int i){
        gamesPlayed[gameNumber].setMinesLeftArray(value,i);
    }

    public void setStatistics() {
        int dif;
        boolean wonGame;
        gamesStarted = 0;
        easyGamesWon = 0;
        mediumGamesWon = 0;
        hardGamesWon = 0;
        easyGamesLost = 0;
        mediumGamesLost = 0;
        hardGamesLost = 0;


        for (int i = 0; i < gamesPlayed.length; i++) {

            if (gamesPlayed[i] != null) {

                gamesStarted++;

                dif = gamesPlayed[i].getDifficulty();
                wonGame = gamesPlayed[i].getResult();

                switch (dif) {

                    case 0:
                        if (wonGame) {
                            easyGamesWon++;
                        } else {
                            easyGamesLost++;
                        }
                        break;

                    case 1:
                        if (wonGame) {
                            mediumGamesWon++;
                        } else {
                            mediumGamesLost++;
                        }
                        break;

                    case 2:
                        if (wonGame) {
                            hardGamesWon++;
                        } else {
                            hardGamesLost++;
                        }
                        break;

                    default:
                        System.out.println("Error on Switch");
                        break;
                } // End of Switch

            } else {
                i = gamesPlayed.length;
            }
        }
    }
} // End of Class Player
