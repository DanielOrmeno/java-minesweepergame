
package mines;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author dormenog
 */
public class MainUI implements Serializable {

    ///////////////////////////////////////////////////////////////////////
    ///////////////////Instance variables for UI///////////////////////////
    ///////////////////////////////////////////////////////////////////////
    private final int WIDTH = 375; // Width and Heigth of the Frame
    private final int HEIGHT = 325;
    private int howDifficult = -1; // Sets the difficulty of the game
    private Player[] players = new Player[200]; //Array of Players
    private Player[] loadedPlayers = new Player[200];
    private int indexOfPlayerArray; //index ++ when creating a new Player
    private int loadIndex; // players array index for loading games.
    private boolean foundPlayer = false; //Flags existing player
    private int foundPlayerIndex;//Sets players[] to found object
    private String loadPlayerName; //Sets name of player when loading a game
    private int loadGameNumber; // Sets number of game when loading a game
    private GridBagConstraints gbc = new GridBagConstraints();
    private ObjectOutputStream os;
    private ObjectInputStream is;
    ///////////////////////////////////////////////////////////////////////
    ///////////////////MainUIPanel Components////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    private JLabel mainUILabel, byLabel;
    private JButton newGameButton, loadGameButton, leaderboardsButton;
    ///////////////////////////////////////////////////////////////////////
    ///////////////////StartGamePanel Components////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    private JFrame mineSweeper;
    private JPanel pane1, pane2, pane3, pane4;
    //Buttons startGamePane
    private JRadioButton easyButton, mediumButton, hardButton;
    private ButtonGroup difficultyGroup;
    private JButton startButton, menuButton1;
    // Labels startGamePane
    private JLabel playerSection;
    private JLabel difficultySection;
    private JLabel selectedDifficulty;
    private JLabel difficultyLabel;
    // Text Fields StartGamePane
    private JTextField nameTextField;
    ///////////////////////////////////////////////////////////////////////
    ///////////////////loadGamePanel Components////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    private JButton menuButton2;
    private JButton loadButton;
    // Strings for dropdown menu.
    // and Drop down menus
    private String[] playerTexts;
    private JComboBox playerMenu;
    private String[] gameTexts;
    private JComboBox gameMenu;
    ///////////////////////////////////////////////////////////////////////
    ///////////////////leaderboardsPanel Components////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    private JButton menuButton3;
    private JButton loadButton2;
    private JPanel easyPanel;
    private JFrame easyBoard;
    private JPanel mediumPanel;
    private JFrame mediumBoard;
    private JPanel hardPanel;
    private JFrame hardBoard;
    private JTextArea easyArea;
    private JTextArea mediumArea;
    private JTextArea hardArea;
    private JLabel easyLabel;
    private JLabel mediumLabel;
    private JLabel hardLabel;
    private JLabel position1;
    private JLabel position2;
    private JLabel position3;
    private JLabel position4;
    private JLabel position5;
    private JLabel position6;
    private JLabel position7;
    private JLabel position8;
    private JLabel position9;
    private JLabel position10;
    private JLabel position11;
    private JLabel position12;
    private JLabel position13;
    private JLabel position14;
    private JLabel position15;
    private String[] playerNames;
    private JComboBox playersStats;
    private JButton getStatistics;
    private JButton easyBoardButton;
    private JButton mediumBoardButton;
    private JButton hardBoardButton;

    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////// Constructors ////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    public MainUI() throws FileNotFoundException, ClassNotFoundException {

        this.indexOfPlayerArray = 0;
        // Frame for UI
        mineSweeper = new JFrame("Minesweeper");
        mineSweeper.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mineSweeper.setSize(WIDTH, HEIGHT);

        //Saves players upon exit.
        mineSweeper.addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                if (players != null) {
                    try {
                        os = new ObjectOutputStream(new FileOutputStream("players.dat"));
                        os.writeObject(players);
                        os.close();
                        System.out.println("players.dat file saved");

                    } catch (IOException ex) {
                        Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        // Loads players
        try {
            is = new ObjectInputStream(new FileInputStream("players.dat"));
            loadedPlayers = (Player[]) is.readObject();
            is.close();
            System.out.println("players.dat file loaded into loadedPlayers Object");

        } catch (IOException ex) {
            Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }

        // Parse loaded players to players Array.
        if (loadedPlayers[0] != null) {
            for (int i = 0; i < loadedPlayers.length; i++) {
                if (loadedPlayers[i] != null) {
                    players[i] = loadedPlayers[i];
                    indexOfPlayerArray = i;
                } else {
                    i = loadedPlayers.length;
                    indexOfPlayerArray++;
                }
            }

            System.out.println("loadedPlayers successfully parsed to players array\n");
        } else {
            indexOfPlayerArray = 0;
        }

        mainUIPanel();

    } // END OF MainUI Constructor

    ///////////////////////////////////////////////////////////////////////
    ///////////////////Main UI Panels /////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    public void mainUIPanel() {

        // Panes
        pane1 = new JPanel(new GridBagLayout());
        gbc.insets = new Insets(10, 10, 10, 10);

        //////////////// Components /////////////////
        mainUILabel = new JLabel("Welcome to MineSweeper");
        byLabel = new JLabel("by Daniel Ormeno");
        newGameButton = new JButton("Start New Game");
        newGameButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                mineSweeper.remove(pane1);
                startGamePanel();
            }
        });

        loadGameButton = new JButton("Load New Game");
        loadGameButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                mineSweeper.remove(pane1);
                loadGamePanel();
            }
        });
        leaderboardsButton = new JButton("Leaderboards");
        leaderboardsButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                mineSweeper.remove(pane1);
                leaderboardsPanel();
            }
        });

        gbc.gridx = 0;
        gbc.gridy = 0;
        pane1.add(mainUILabel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        pane1.add(byLabel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        pane1.add(newGameButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        pane1.add(loadGameButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        pane1.add(leaderboardsButton, gbc);

        mineSweeper.add(pane1, BorderLayout.CENTER);
        mineSweeper.setVisible(true);
        mineSweeper.setResizable(false);
    } // END OF ManIUPanel

    public void startGamePanel() {

        // Panes
        pane2 = new JPanel(new GridBagLayout());
        gbc.insets = new Insets(10, 10, 10, 10);

        //////////////// Components /////////////////
        //Labels

        playerSection = new JLabel("Enter Player Name: ");
        difficultySection = new JLabel("Game Difficulty:   ");
        selectedDifficulty = new JLabel("");
        difficultyLabel = new JLabel("How hard can it be?: ");

        // Text Field 
        nameTextField = new JTextField(8);


        /////////// Buttons ////////
        // Back to Menu Button

        menuButton1 = new JButton("Back to Menu");
        menuButton1.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                mineSweeper.remove(pane2);
                mainUIPanel();
            }
        });
        // Start Game Button
        startButton = new JButton("Start Game");

        // Difficulty Radio Buttons.

        easyButton = new JRadioButton("Easy");
        easyButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                howDifficult = 0;
                selectedDifficulty.setText("Just 40 mines");
            }
        });

        mediumButton = new JRadioButton("Medium");
        mediumButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                howDifficult = 1;
                selectedDifficulty.setText("100 lovely mines");
            }
        });
        hardButton = new JRadioButton("Hard");
        hardButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                howDifficult = 2;
                selectedDifficulty.setText("Imposibru 200 mines");
            }
        });

        // Button Group (Only one difficulty can be selected at a time)

        difficultyGroup = new ButtonGroup();
        difficultyGroup.add(easyButton);
        difficultyGroup.add(mediumButton);
        difficultyGroup.add(hardButton);


        // Layout First Column

        gbc.anchor = GridBagConstraints.LINE_END;

        gbc.gridx = 0;
        gbc.gridy = 1;
        pane2.add(playerSection, gbc);


        gbc.gridx = 0;
        gbc.gridy = 3;
        pane2.add(difficultySection, gbc);

        gbc.gridx = 0;
        gbc.gridy = 6;
        pane2.add(difficultyLabel, gbc);

        // Layout Second Column

        gbc.anchor = GridBagConstraints.LINE_START;

        gbc.gridx = 1;
        gbc.gridy = 1;
        pane2.add(nameTextField, gbc);

        gbc.gridx = 1;
        gbc.gridy = 3;
        pane2.add(easyButton, gbc);

        gbc.gridx = 1;
        gbc.gridy = 4;
        pane2.add(mediumButton, gbc);

        gbc.gridx = 1;
        gbc.gridy = 5;
        pane2.add(hardButton, gbc);

        gbc.gridx = 1;
        gbc.gridy = 6;
        pane2.add(selectedDifficulty, gbc);

        // Layout last Row
        gbc.anchor = GridBagConstraints.LINE_START;

        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.weighty = 50;
        pane2.add(startButton, gbc);

        gbc.gridx = 1;
        gbc.gridy = 7;
        gbc.weighty = 50;
        pane2.add(menuButton1, gbc);

        /////////////////
        mineSweeper.add(pane2, BorderLayout.CENTER);
        mineSweeper.setVisible(true);
        mineSweeper.setResizable(false);

        ////////////// EVENT HANDLING ///////////


        startButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                String playerName = nameTextField.getText();

                if ((playerName.equals("")) || (howDifficult == -1)) {
                    JOptionPane.showMessageDialog(null, "Please select Player Name and Difficulty");
                    return;
                } else if (indexOfPlayerArray == 0) {
                    System.out.println("PlayerArray Initiated");
                    System.out.println("");
                } else {
                    for (int ind = 0; ind < indexOfPlayerArray; ind++) {
                        if (players[ind].getPlayerName().equals(nameTextField.getText())) {
                            JOptionPane.showMessageDialog(null, "This player already exits. Game will be added to player's Statistics!");
                            foundPlayer = true;
                            foundPlayerIndex = ind;
                        }
                    }
                }

                if (foundPlayer) {
                    players[foundPlayerIndex].newGame(howDifficult);
                    foundPlayer = false;
                    foundPlayerIndex = -1;
                } else {
                    System.out.println("New player detected");
                    if (indexOfPlayerArray <= players.length) {
                        players[indexOfPlayerArray] = new Player(playerName, howDifficult);
                        indexOfPlayerArray++;
                    } else {
                        indexOfPlayerArray = 0;
                        JOptionPane.showMessageDialog(null, "Maximun Number of players reached, will ovewrite");
                        indexOfPlayerArray = 0;
                    }
                }
            }
        });
    } // END OF startGamePanel

    public void loadGamePanel() {

        pane3 = new JPanel(new GridBagLayout());

        gbc.insets = new Insets(10, 10, 10, 10);

        JLabel nameLabel = new JLabel("Select Player:");
        JLabel gameLabel = new JLabel("Select Game:");

        int i = -1;

        // Finds how many players exist.
        do {
            i++;
        } while (players[i] != null);


        playerTexts = new String[i];

        // Fills boxTexts Array
        for (int j = 0; j < i; j++) {
            if (j == 0) {
                playerTexts[j] = "Select Player";
            } else {

                playerTexts[j] = players[j].getPlayerName();
            }
        }

        playerMenu = new JComboBox(playerTexts);

        // Gets player name from Menu
        playerMenu.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                String loadPlayer;
                JComboBox cb = (JComboBox) ae.getSource();
                loadPlayer = (String) cb.getSelectedItem();

                if (loadPlayer.equals("Select Player")) {

                    JOptionPane.showMessageDialog(null, "Please Select Player");
                    return;
                }
                setLoadPlayerName(loadPlayer);
                System.out.println("Player " + loadPlayer + " Selected.");
                setGameTexts(loadPlayer);
                gameMenu = new JComboBox(gameTexts);
                gameMenu.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        int gameNumber;
                        JComboBox cb = (JComboBox) ae.getSource();
                        gameNumber = Integer.parseInt((String) cb.getSelectedItem());
                        setLoadGameNumber(gameNumber);
                    }
                });

                gbc.anchor = GridBagConstraints.LINE_START;
                gbc.gridx = 1;
                gbc.gridy = 1;
                pane3.add(gameMenu, gbc);
                mineSweeper.setVisible(true);
            }
        });


        menuButton2 = new JButton("Back to Menu");
        menuButton2.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                mineSweeper.remove(pane3);
                mainUIPanel();
            }
        });

        loadButton = new JButton("Load Game");

        loadButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {

                if (loadPlayerName == "" || loadGameNumber < 0) {
                    JOptionPane.showMessageDialog(null, "Please Select Player and Game");
                    return;
                } else {

                    loadGame(loadPlayerName, loadGameNumber);
                    mineSweeper.remove(pane3); //Resets Pannel;
                    loadGamePanel();
                    loadPlayerName = "";
                    loadGameNumber = -1;
                }
            }
        });

        menuButton2 = new JButton("Back to Menu");
        menuButton2.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                mineSweeper.remove(pane3);
                mainUIPanel();
            }
        });

        gbc.anchor = GridBagConstraints.LINE_END;

        gbc.gridx = 0;
        gbc.gridy = 0;
        pane3.add(nameLabel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        pane3.add(gameLabel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        pane3.add(loadButton, gbc);

        gbc.anchor = GridBagConstraints.LINE_START;

        gbc.gridx = 1;
        gbc.gridy = 0;
        pane3.add(playerMenu, gbc);


        // game menu added on playerMenu action listener

        gbc.gridx = 1;
        gbc.gridy = 2;
        pane3.add(menuButton2, gbc);


        /////////////////
        mineSweeper.add(pane3, BorderLayout.CENTER);
        mineSweeper.setVisible(true);
        mineSweeper.setResizable(true);


    } // END OF loadGamePanel

    public void leaderboardsPanel() {

        // Updates statistics
        int num = 0;
        while (players[num] != null) {
            players[num].setStatistics();
            num++;
        }
        System.out.println("Player Statistics Updated\n");

        pane4 = new JPanel(new GridBagLayout());

        easyPanel = new JPanel(new GridBagLayout());

        mediumPanel = new JPanel(new GridBagLayout());
        hardPanel = new JPanel(new GridBagLayout());

        menuButton3 = new JButton("Back to Menu");
        menuButton3.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                mineSweeper.remove(pane4);
                mainUIPanel();
            }
        });

        easyBoardButton = new JButton("LeaderBoard");
        easyBoardButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {

                int max = 1000;
                int number = 1;
                String player = "";

                player = createLeaderBoard(0, max);
                if (player.equals("****")) {
                    easyBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getEasyGamesWon();
                position1.setText(number + ". " + player);
                number++;

                player = createLeaderBoard(0, max);
                if (player.equals("****")) {
                    easyBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getEasyGamesWon();
                position2.setText(number + ". " + player);
                number++;

                player = createLeaderBoard(0, max);
                if (player.equals("****")) {
                    easyBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getEasyGamesWon();
                position3.setText(number + ". " + player);
                number++;

                player = createLeaderBoard(0, max);
                if (player.equals("****")) {
                    easyBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getEasyGamesWon();
                position4.setText(number + ". " + player);
                number++;

                player = createLeaderBoard(0, max);
                if (player.equals("****")) {
                    easyBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getEasyGamesWon();
                position5.setText(number + ". " + player);

                easyBoard.setVisible(true);
            }
        });
        mediumBoardButton = new JButton("Leaderboard");
        mediumBoardButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                int max = 1000;
                int number = 1;
                String player = "";

                player = createLeaderBoard(1, max);
                if (player.equals("****")) {
                    mediumBoard.setVisible(true);
                    System.out.println("Its happening here");
                    return;
                }
                max = players[getNameIndex(player)].getMediumGamesWon();
                position6.setText(number + ". " + player);
                number++;

                player = createLeaderBoard(1, max);
                if (player.equals("****")) {
                    mediumBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getMediumGamesWon();
                position7.setText(number + ". " + player);
                number++;

                player = createLeaderBoard(1, max);
                if (player.equals("****")) {
                    mediumBoard.setVisible(true);;
                    return;
                }
                max = players[getNameIndex(player)].getMediumGamesWon();
                position8.setText(number + ". " + player);
                number++;

                player = createLeaderBoard(1, max);
                if (player.equals("****")) {
                    mediumBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getMediumGamesWon();
                position9.setText(number + ". " + player);
                number++;

                player = createLeaderBoard(1, max);
                if (player.equals("****")) {
                    mediumBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getMediumGamesWon();
                position10.setText(number + ". " + player);
                mediumBoard.setVisible(true);
            }
        });

        hardBoardButton = new JButton("Leaderboard");
        hardBoardButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                int max = 1000;
                int number = 1;
                String player = "";

                player = createLeaderBoard(2, max);
                if (player.equals("****")) {
                    hardBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getHardGamesWon();
                position11.setText(number + ". " + player);
                number++;

                player = createLeaderBoard(2, max);
                if (player.equals("****")) {
                    hardBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getHardGamesWon();
                position12.setText(number + ". " + player);
                number++;

                player = createLeaderBoard(2, max);
                if (player.equals("****")) {
                    hardBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getHardGamesWon();
                position13.setText(number + ". " + player);
                number++;

                player = createLeaderBoard(2, max);
                if (player.equals("****")) {
                    hardBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getHardGamesWon();
                position14.setText(number + ". " + player);
                number++;

                player = createLeaderBoard(2, max);
                if (player.equals("****")) {
                    hardBoard.setVisible(true);
                    return;
                }
                max = players[getNameIndex(player)].getHardGamesWon();
                position15.setText(number + ". " + player);

                hardBoard.setVisible(true);
            }
        });


        /////////////////// Labels for leadeboards ///////////////////////////
        easyLabel = new JLabel("Easy");
        mediumLabel = new JLabel("Medium");
        hardLabel = new JLabel("Hard");
        position1 = new JLabel("1. ");
        position2 = new JLabel("2. ");
        position3 = new JLabel("3. ");
        position4 = new JLabel("4. ");
        position5 = new JLabel("5. ");
        position6 = new JLabel("1. ");
        position7 = new JLabel("2. ");
        position8 = new JLabel("3. ");
        position9 = new JLabel("4. ");
        position10 = new JLabel("5. ");
        position11 = new JLabel("1. ");
        position12 = new JLabel("2. ");
        position13 = new JLabel("3. ");
        position14 = new JLabel("4. ");
        position15 = new JLabel("5. ");

        /////////////////// Get Statistics Section //////////////////////////

        int i = -1;

        // Finds how many players exist.
        do {
            i++;
        } while (players[i] != null);


        playerNames = new String[i];

        // Fills boxTexts Array
        for (int j = 0; j < i; j++) {
            if (j == 0) {
                playerNames[j] = "Select Player";
            } else {

                playerNames[j] = players[j].getPlayerName();
            }
        }

        playersStats = new JComboBox(playerNames);

        // Gets player name from Menu
        playersStats.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {

                String loadPlayer;
                JComboBox cb = (JComboBox) ae.getSource();
                loadPlayer = (String) cb.getSelectedItem();

                if (loadPlayer.equals("Select Player")) {

                    JOptionPane.showMessageDialog(null, "Please Select Player");
                    return;
                }
                setLoadPlayerName(loadPlayer);
                System.out.println("Player " + loadPlayer + " Selected.");
            }
        });

        getStatistics = new JButton("Get Statistics");
        getStatistics.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {

                if (loadPlayerName == "") {
                    JOptionPane.showMessageDialog(null, "Please Select Player");
                    return;
                }

                for (int num = 0; num < players.length; num++) {

                    if (players[num] != null) {
                        if (players[num].getPlayerName().equals(loadPlayerName)) {
                            JOptionPane.showMessageDialog(null, players[num].getStatistics());
                        }
                    } else {
                        num = players.length; // Exits loop
                    }
                }
                loadPlayerName = "";
            }
        });

        /////////////////// Panels and Components //////////////////////////////
        gbc.insets = new Insets(10, 10, 10, 10);

        gbc.anchor = GridBagConstraints.LINE_END;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridx = 1;
        gbc.gridy = 0;
        pane4.add(new JLabel("LEADERBOARDS (Top 5 Players)"), gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        pane4.add(easyLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        pane4.add(easyBoardButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        pane4.add(mediumLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 2;
        pane4.add(mediumBoardButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        pane4.add(hardLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 3;
        pane4.add(hardBoardButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        pane4.add(playersStats, gbc);

        gbc.gridx = 1;
        gbc.gridy = 4;
        pane4.add(getStatistics, gbc);

        gbc.gridx = 1;
        gbc.gridy = 5;
        pane4.add(menuButton3, gbc);

        ///////////////////////

        gbc.gridx = 0;
        gbc.gridy = 0;
        easyPanel.add(position1, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        easyPanel.add(position2, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        easyPanel.add(position3, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        easyPanel.add(position4, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        easyPanel.add(position5, gbc);

        gbc.gridx = 0;
        gbc.gridy = 0;
        mediumPanel.add(position6, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        mediumPanel.add(position7, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        mediumPanel.add(position8, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        mediumPanel.add(position9, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        mediumPanel.add(position10, gbc);

        gbc.gridx = 0;
        gbc.gridy = 0;
        hardPanel.add(position11, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        hardPanel.add(position12, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        hardPanel.add(position13, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        hardPanel.add(position14, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        hardPanel.add(position15, gbc);

        easyBoard = new JFrame("Easy - Top 5 Players");
        easyBoard.setSize(150, 300);
        easyBoard.add(easyPanel);

        mediumBoard = new JFrame("Medium - Top 5 Players");
        mediumBoard.setSize(150, 300);
        mediumBoard.add(mediumPanel);

        hardBoard = new JFrame("Hard - Top 5 Players");
        hardBoard.setSize(150, 300);
        hardBoard.add(hardPanel);

        mineSweeper.add(pane4);//, BorderLayout.CENTER);
        mineSweeper.setVisible(true);
        mineSweeper.setResizable(false);

    } // END OF leaderboardsPanel

    ///////////////////////////////////////////////////////////////////////
    /////////////////// Main UI Methods ///////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    public void loadGame(String playerName, int gameNumb) {

        String fileName = (playerName + "-" + gameNumb + ".txt");
        System.out.println("Loading file: " + fileName);
        System.out.println(" ");

        File toLoad = new File(fileName);
        int gameNumber;
        int length;
        int difficulty;
        int instance;
        int currentInstance;
        int health;
        String name;
        boolean endOfInstance = false;

        try {
            BufferedReader fr = new BufferedReader(new FileReader(toLoad));

            name = fr.readLine(); //Gets player name from file
            System.out.println("reads from file " + name);
            gameNumber = Integer.parseInt(fr.readLine()); // Gets number of game from file
            difficulty = Integer.parseInt(fr.readLine()); // gets difficulty integer
            length = Integer.parseInt(fr.readLine()); // Gets length of Instance arrayi
            instance = Integer.parseInt(fr.readLine()); // Gets number of instances saved
            currentInstance = Integer.parseInt(fr.readLine()); // Gets number of current instance when game was saved.
            health = Integer.parseInt(fr.readLine());// Gets Health
            // Checks for player name in Players array and sets loadIndex;
            for (int i = 0; i < players.length; i++) {
                if (players[i].getPlayerName().equals(name)) {
                    loadIndex = i;
                    System.out.println("Player " + name + " found in Index " + loadIndex);
                    i = players.length; //Exits loop
                }
            }


            // Loads existing game of player
            players[loadIndex].loadGame(gameNumber, difficulty);

            // Modifies the gameInstance array
            for (int k = 0; k < instance; k++) {

                for (int i = 0; i < length; i++) {

                    if ((i == length - 1) && (k == instance - 1)) {
                        endOfInstance = true;
                    }
                    players[loadIndex].setPlayerArray(gameNumber, Integer.parseInt(fr.readLine()), i, k, currentInstance, endOfInstance, health);
                }
            }

            for (int k = 0; k < instance; k++) {
                players[loadIndex].setPlayerHealthInstance(gameNumber, Integer.parseInt(fr.readLine()), k);
            };

            for (int k = 0; k < instance; k++) {
                players[loadIndex].setPlayerMinesLeftInstance(gameNumber, Integer.parseInt(fr.readLine()), k);
            }


            fr.close();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Game was not saved to disk");
        }

    }

    public void setLoadPlayerName(String name) {

        this.loadPlayerName = name;
    }

    public void setLoadGameNumber(int n) {
        this.loadGameNumber = n;
    }

    public void setGameTexts(String name) {
        // Looks up number of games played by the selected player
        for (int j = 0; j < players.length; j++) {

            if (players[j].getPlayerName().equals(name)) {
                // Finds the player and creates a string with lengh equal to the games played index
                gameTexts = new String[players[j].getIndex()];
                // Fills the array
                for (int k = 0; k < gameTexts.length; k++) {
                    gameTexts[k] = Integer.toString(k);
                }
                j = players.length; // Exits loop.
            }
        }
    }

    public String createLeaderBoard(int dif, int top) {

        String tempName = "";
        int tempMax = 0;
        boolean foundMax = false;

        switch (dif) {
            case 0:
                for (int i = 0; i < players.length; i++) {

                    if (players[i] != null) {

                        if (players[i].getEasyGamesWon() > tempMax && players[i].getEasyGamesWon() < top) {
                            tempMax = players[i].getEasyGamesWon();
                            tempName = players[i].getPlayerName();
                            foundMax = true;
                        }

                    } else {
                        i = players.length; //Exits loop.
                    }
                    if (!foundMax) {
                        tempName = "****";
                    }
                }
                break;
            case 1:
                for (int i = 0; i < players.length; i++) {

                    if (players[i] != null) {

                        if (players[i].getMediumGamesWon() > tempMax && players[i].getMediumGamesWon() < top) {
                            tempMax = players[i].getMediumGamesWon();
                            tempName = players[i].getPlayerName();
                            foundMax = true;
                        }
                    } else {
                        i = players.length; //Exits loop.
                    }
                    if (!foundMax) {
                        tempName = "****";
                    }
                }
                break;
            case 2:
                for (int i = 0; i < players.length; i++) {

                    if (players[i] != null) {

                        if (players[i].getHardGamesWon() > tempMax && players[i].getHardGamesWon() < top) {
                            tempMax = players[i].getHardGamesWon();
                            tempName = players[i].getPlayerName();
                            foundMax = true;
                        }
                    } else {
                        i = players.length; //Exits loop.
                    }
                    if (!foundMax) {
                        tempName = "****";
                    }
                }
                break;
        }

        return tempName;
    }

    public int getNameIndex(String name) {
        int num = 99090909;

        for (int i = 0; i < players.length; i++) {
            if (players[i].getPlayerName().equals(name)) {
                return i;
            }
        }
        return num;
    }

    public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException {
        System.out.println("MainUI started");
        System.out.println("");
        new MainUI();
    }
} // END OF MainUI CLASS
